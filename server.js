const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const viewersRoutes = require('./routes/viewer.js');


const api = express();
api.use(cors());

//connect to mongodb atlas via mongoose
mongoose.connect('mongodb+srv://admin:rtPgDHug1rSobzDW@wdc028-course-booking.0dvhv.mongodb.net/dbBooking_app?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false
});

//set notification for connection mongodb if success/failure
let db = mongoose.connection;


//error handling
//error tracing
//if connection error encountered, output it in console
db.on('error', (console.error.bind(console, 'connection error: ')));

//once connected, show notif
db.once('open', () => console.log("We're connected to our database."));

// mongoose {
// 	connection {
// 		status: 'error'
// 		once: () => {}
// 		on: () => {}
// 	}
// }


//middleware - built in function of app
//express.json() - it has a features of codes that we can use. It tells our app that all request that we will get is a json format
api.use(express.json()); //the whole is an object .use is a method

//"{name: 'eat', status: 'pending'}"
//if we remove the quotation, it is actually an object
//so how the server will know the specific data that we are getting. It has a parsing to analyze a string/text and covert it to an object to make it readabale to our database
//req.body

api.use(express.urlencoded({extended:true}));
/* destructure 
api {
	use: () => {}
}

express {
	urlencoded: (extended:true) => {}
}
*/


//define schema - structure of documents
//when we want to create a schema this is done via schema() constructor of the mongoose module
const taskSchema = new mongoose.Schema({
	name: String,
/* 
name: {
	type: String,
	required: [true, 'Category type is required']
}
*/
	status: {
		type: String,
		default: 'pending'
	}
})

const Task = mongoose.model('Task', taskSchema);
//MVC = model(blueprints), views(userinterface), controllers
/*
taskSchema {
	save: () => 
}
schema: () => 
*/


//mini act
/*
create a user schema using mongoose with the following fields
username: string
email: string
password

task is an array of embeded task documents, it refers to taskSchema
tasks: array
*/

const userSchema = new mongoose.Schema({
	username: String,
	email: String,
	password: String,
	tasks: [
		taskSchema
	]
})

const User = mongoose.model('User', userSchema);


//request
/*
	<body>
		<form>
			<input username
		</form>
	</body>

api.get('/', (req, res) => {
	{
		username: req.body.username
	},
	{
		email: req.body.email
	}
})
*/															


//register a new user
api.post('/user', (req, res)=> {
	//1. check if username or email are duplicate prior to registration

	//User == UserSchema 
	//.find() mongoose method 

	/*
	{ $or: performs a logical OR operator in an array
		[
		{ username: req.body.username},
		{ email: req.body.email}
		]
	}
	*/
	User.find({ $or: [{ username: req.body.username }, { email: req.body.email }] }, (findErr, duplicates) => {
			//2. if error return
			if(findErr) return console.error(findErr);
			//3. if duplicates found
			if(duplicates.length > 0) {
				return res.status(403).json({
					message: "Duplicates found, kindly choose different username and/or email."
				}) //4. if no duplicates found save the data

			} else {
			//5. instantiate a new user object with properties derived from the request body
			let newUser = new User({
				username: req.body.username,
				email: req.body.email,
				password: req.body.password,
				//tasks is initially an empty array
				tasks: []
			})
			//6. save hte new user
			newUser.save((saveErr, newUser) => {
				//if an error was ecncounter while saving the document - notification in the console
				if(saveErr) return console.error(saveErr);
				return res.status(201).json({
					//status 201, successful creation
					//status 200-299 success response
					//400-499 related to client
					//500 and above error connecting to your server
						message: `User ${newUser.username} successfull registered,`,
						data: {
							username: newUser.username,
							email: newUser.email,
							link_to_self: `/user/${newUser._id}`
						}
				});
			})
		}
	})	
});

//display user
//1. routes
//2. find a specific user
//3. if error return error
//4. else return user retrieved successfully


api.get('/user/:id', (req, res) => {
	User.findById(req.params.id, (err, user) => {
		if(err) return cnsole,error(err);
		return res.status(200).json({
			message: "User retrived successfully." , 
			data: {
				username: user.username,
				email: user.email,
				tasks: `/users/${user._id}`
			}
		})
	})
})

//create a new task for a specific user

// 1. let's create a route 
	api. post('/user/:userId/tasks', (req, res) => {


// 2. find the using the query findById
	User.findById(req.params.userId, (findErr, user) => {
// 3. If error, return error
	if(findErr) return console.error(findErr);
// 4. If the user currently has no task, add a new task 
	if(user.tasks === []) { //user.tasks.length === 0
		//add the new task as a subdocument to the tasks array
			user.tasks.push({
				name: req.body.name
			});
			user.save((saveErr, modifiedUser) => {
				if(saveErr) return console.error(saveErr);
				return res.status(200).json({
					message: `${user.tasks[0].name} added to task list of ${user.username}`,
					data: modifiedUser.tasks[0]
				})
			});
		} else {
// 5. else, look for duplicate, if may duplicate, return the task is already registered
			let dupes = user.tasks.filter(task => task.name.toLowerCase() === req.body.name.toLowerCase());

			if(dupes.length > 0) {
				return res.status(403).json({
					message: `$(req.body.name} is already registered as a task.`
				})
			}
		else{
// 6. else if no duplicates found, save the task
			user.tasks.push({
				name: req.body.name
			});
			user.save((saveErr, modifiedUser) => {
				if(saveErr) return console.error(saveErr);
				return res.status(200).json({
					message: `Added new task successfully`,
					//sample: [{eat}, {sleep}, {code}]
					data: modifiedUser.tasks[modifiedUser.tasks.length-1]
				})
			});
		}
	}
	})
	})

//get details of a specific task of a user
//1. create routes
//2. find the specific user, if error, return the error
//3. access the specific task of a user if task is null, return cannot be found
//4. else return task with id <taskId> found


//routes
api.use('/', viewersRoutes);


//create port - communication - 3000
const port = 3000;
api.listen(port, () => {console.log(`Listening on port ${port}.`)});