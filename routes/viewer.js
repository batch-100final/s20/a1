const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

router.get('/get', (request, response) => {
	response.send('List of Viewers')
});


//get
router.get('/fetch', (req, res) => {
	res.json({
		"name": "Juan",
		"gender": "male"
	})
});


//post
// router.post('/insert', (req, res) => {
// 	res.json({
// 		"message": "Success!"
// 	})
// });


//request.query
router.get('/req', (req, res) => {
	res.send(req.query)

});

//body
router.post('/insert', (req, res) => {
	console.log(req.body);
});


//destructure
router.get('/destruct', (req,res) => {
	const { name, age, gender} = req.query

	if(age === 24) {
		res.status(200).send({
			name,
			age,
			gender
		})
	} else {
		res.status(500).send({
			"message": "Age is not equal.",
			"status": "Failed"
		})
	}
});

module.exports = router;
